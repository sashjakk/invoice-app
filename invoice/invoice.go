package invoice

import (
	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"time"
)

const invalidClientName = "invalid"

var (
	DefaultDealer = BusinessParty{
		Name:         "SV Engineering Ltd",
		Street:       "100 Earl St.",
		City:         "Northampton",
		PostalCode:   "NN1 3AX",
		PhoneNumber1: "+44 7925 721136",
		Email:        "svenglimited@gmail.com",
	}

	DefaultClient = BusinessParty{
		Name: invalidClientName,
	}
)

type BusinessParty struct {
	Name         string `yaml:"name"`
	Street       string `yaml:"street"`
	City         string `yaml:"city"`
	PostalCode   string `yaml:"postalCode"`
	PhoneNumber1 string `yaml:"phoneNumber1"`
	PhoneNumber2 string `yaml:"phoneNumber2"`
	Email        string `yaml:"email"`
}

type BusinessItem struct {
	Description string  `yaml:"description"`
	Amount      int     `yaml:"amount"`
	UnitPrice   float64 `yaml:"unitPrice"`
	Total       float64 `yaml:"-"`
}

type Invoice struct {
	Number   int64          `yaml:"invoiceNumber"`
	Date     Date           `yaml:"date"`
	Tax      float64        `yaml:"tax"`
	Dealer   BusinessParty  `yaml:"dealer"`
	Client   BusinessParty  `yaml:"client"`
	Items    []BusinessItem `yaml:"services"`
	SubTotal float64        `yaml:"-"`
	Total    float64        `yaml:"-"`
	TaxTotal float64        `yaml:"-"`
}

func NewInvoice() *Invoice {
	now := time.Now()
	return &Invoice{
		Number: now.Unix(),
		Date:   Date{&now},
		Tax:    20,
		Dealer: DefaultDealer,
		Client: DefaultClient,
		Items:  []BusinessItem{},
	}
}

func NewInvoiceFromYaml(source string) (*Invoice, error) {
	i := NewInvoice()

	err := yaml.Unmarshal([]byte(source), &i)
	if err != nil {
		return NewInvoice(), errors.Wrap(err, "failed to parse yaml")
	}

	if err := validate(i); err != nil {
		return NewInvoice(), errors.Wrap(err, "failed to validate invoice")
	}

	i.Calculate()

	return i, nil
}

func NewInvoiceFromFile(path string) (*Invoice, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return &Invoice{}, errors.Wrap(err, "failed to read file")
	}

	return NewInvoiceFromYaml(string(data))
}

func (i *Invoice) Calculate() {

	// sum up all item prices
	for index, item := range i.Items {
		i.Items[index].Total = item.UnitPrice * float64(item.Amount)
		i.SubTotal += i.Items[index].Total
	}

	// calculate price with taxes
	i.TaxTotal = (i.Tax / 100) * i.SubTotal
	i.Total += i.SubTotal + i.TaxTotal
}

func validate(i *Invoice) error {
	if i.Client.Name == invalidClientName {
		return errors.New("client information not provided")
	}

	if len(i.Items) == 0 {
		return errors.New("no service details provided")
	}

	return nil
}
