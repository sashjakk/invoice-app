package invoice

import (
	"github.com/pkg/errors"
	"time"
)

type Date struct {
	*time.Time
}

func (d *Date) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var date string
	if err := unmarshal(&date); err != nil {
		return errors.Wrap(err, "unable to parse date")
	}

	t, err := time.Parse("02/01/2006", date)
	if err != nil {
		return errors.Wrap(err, "invalid date format")
	}

	*d = Date{&t}

	return nil
}
