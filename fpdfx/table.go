package fpdfx

import (
	"github.com/jung-kurt/gofpdf"
)

type TableCell struct {
	Text  string
	Width float64
	Style *TableCellStyle
}

type TableCellStyle struct {
	LineHeight float64
	Border     string
	Align      string
}

func Table(p *gofpdf.Fpdf, cells [][]TableCell) {
	left, right, _, _ := p.GetMargins()
	width, _ := p.GetPageSize()
	width = width - left - right

	for _, row := range cells {
		for _, cell := range row {
			p.CellFormat(
				width*cell.Width,
				cell.Style.LineHeight,
				cell.Text,
				cell.Style.Border,
				0,
				cell.Style.Align,
				false,
				0,
				"",
			)
		}
		p.Ln(-1)
	}
}
