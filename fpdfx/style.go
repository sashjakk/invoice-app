package fpdfx

const (
	DefaultFont            = "Courier"
	DefaultFontSize        = 10.0
	DefaultLineHeight      = 5.0
	DefaultTableLineHeight = 8.0
)

const (
	FontStyleRegular = ""
	FontStyleBold    = "B"
)

const (
	BorderFull  = "1"
	BorderEmpty = ""
)

const (
	AlignLeft   = "L"
	AlignRight  = "R"
	AlignCenter = "C"
)

var (
	HeaderStyle = &TableCellStyle{
		LineHeight: TableLineHeight,
		Border:     BorderFull,
		Align:      AlignLeft,
	}

	BodyStyle = &TableCellStyle{
		LineHeight: TableLineHeight,
		Border:     BorderFull,
		Align:      AlignLeft,
	}

	EmptyStyle = &TableCellStyle{
		LineHeight: TableLineHeight,
		Border:     BorderEmpty,
		Align:      AlignRight,
	}

	RightStyle = &TableCellStyle{
		LineHeight: TableLineHeight,
		Border:     BorderFull,
		Align:      AlignRight,
	}
)

var (
	Font               = DefaultFont
	FontSize           = DefaultFontSize
	LineHeight         = DefaultLineHeight
	TableLineHeight    = DefaultTableLineHeight
	HeaderBottomMargin = 10.0
)
