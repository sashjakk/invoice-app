package fpdfx

import (
	"github.com/jung-kurt/gofpdf"
)

func WriteLn(p *gofpdf.Fpdf, text string) {
	if len(text) > 0 {
		p.Cell(0, LineHeight, text)
		p.Ln(-1)
	}
}

func NewLn(p *gofpdf.Fpdf) {
	p.Ln(-1)
}
