# Env setup
SHELL := /bin/sh
MAKEFLAGS += --jobs=2

# App
app = invoice-app
sources = $(shell find . -name "*.go" ! -path "*/vendor/*")
examples = $(wildcard examples/*.yaml)

# Vendor
vendor = vendor
dependencies = Gopkg.toml

# Build
build_dir = build
platforms = windows darwin
platform_ext = .exe
artifacts = $(join $(platforms:%=$(build_dir)/%/$(app)), $(platform_ext))

all: build examples

# --------------------
# Cross platform build
# --------------------
.PHONY: build
build: $(artifacts)

$(artifacts): $(dependencies) $(sources)
	$(eval os = $(word 2, $(subst /, ,$@)))
	GOOS=$(os) GOARCH=amd64 go build -o $@ -v
# --------------------


# --------
# Clean up
# --------
.PHONY: clean
clean:
	rm -rf build/
	rm -rf examples/*.pdf
	rm -rf vendor/
	go clean
# --------


# --------------------
# Install dependencies
# --------------------
$(dependencies): | $(vendor)

$(vendor):
	dep ensure -v
# --------------------


# ---------------
# Create examples
# ---------------
.PHONY: examples
examples: $(examples)

$(examples): build
	$(eval os = $(shell uname -s | awk '{print tolower($0)}'))
	./$(build_dir)/$(os)/$(app) -invoice $@ -output ${PWD}/examples
# ---------------
