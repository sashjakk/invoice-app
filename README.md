# PDF Invoice Generator

Go utility to generate PDF invoices from `.yaml` files
using [FPDF](https://github.com/jung-kurt/gofpdf). 

## Build

```makefile
make
```

## Usage

* Unix

```go
./build/darwin/invoice-app -invoice examples/invoice.yml -output /Users/sashjakk/Desktop
```

* Windows
```go
```

### Command line args

| Name | Description | Example
| --- | --- | --- |
| invoice | path to invoice file in `.yaml` file format | -invoice examples/invoice.yml |
| output | path to directory where report should be stored in `.pdf` format | -output /Users/sashjakk/Desktop


## Input file data fields

* General 

| Name | Mandatory | Default value |
| --- | --- | --- |
| invoiceNumber | no | 1540497625 (current timestamp on moment of generation) |
| data | no | 25/10/2018 (time format - MM/DD/YYYY)
| tax | no | 20 (value in percents)

* Business Party - Dealer

| Name | Mandatory | Default value |
| --- | --- | --- |
| name | no | SV Engineering |
| street | no | 100 Earl St. |
| city | no | Northampton |
| postalCode | no | NN1 3AX |
| phoneNumber1 | no | +44 7925 721136 |
| phoneNumber2 | no | - |
| email | no | svenglimited@gmail.com |

* Business Party - Client

| Name | Mandatory | Default value |
| --- | --- | --- |
| name | yes | - |
| street | yes | - |
| city | yes | - |
| phoneNumber1 | yes | - |
| phoneNumber2 | yes | - |
| email | yes | - |

## Invoice examples

* Minimal 

```yaml
client:
  name: Alex Tomatov
  street: 346 Saules Strt
  city: London
  postalCode: NN1 89Y
  phoneNumber1: +371 1234563
  email: alex@tomat.io
services:
- description: Oil change
  amount: 1
  unitPrice: 25
- description: Car repair
  amount: 1
  unitPrice: 50
```

* Full

```yaml
invoiceNumber: 001
date: 02/03/2018
tax: 20
dealer:
  name: SV Engineering
  street: 100 Earl St.
  city: Northampton
  postalCode: NN1 3AX
  phoneNumber1: +44 7925 721136
  phoneNumber2: ""
  email: svenglimited@gmail.com
client:
  name: Alex Tomatov
  street: 346 Saules Strt
  city: London
  postalCode: NN1 89Y
  phoneNumber1: +371 1234563
  phoneNumber2: ""
  email: alex@tomat.io
services:
- description: Oil change
  amount: 1
  unitPrice: 25
- description: Car repair
  amount: 1
  unitPrice: 50
```

