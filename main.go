package main

import (
	"bitbucket.org/sashjakk/invoice-app/fpdfx"
	"bitbucket.org/sashjakk/invoice-app/invoice"
	"flag"
	"fmt"
	"github.com/jung-kurt/gofpdf"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

var (
	invPath = flag.String(
		"invoice",
		"",
		"provide path to invoice file, ex.: -invoice=/clients/invoice-1.yml",
	)

	outDir = flag.String(
		"output",
		"",
		"specify output directory, ex.: -output=~/Desktop",
	)
)

func main() {
	flag.Parse()
	if *invPath == "" {
		flag.PrintDefaults()
		return
	}

	if *outDir == "" {
		var err error
		*outDir, err = os.Getwd()
		if err != nil {
			log.Fatal(err)
		}
	}

	bill, err := invoice.NewInvoiceFromFile(*invPath)
	if err != nil {
		log.Fatal(err)
	}

	err = pdf(bill)
	if err != nil {
		log.Fatal(err.Error())
	}
}

func pdf(i *invoice.Invoice) error {
	p := gofpdf.New("P", "mm", "A4", "")

	p.SetFont(fpdfx.Font, fpdfx.FontStyleRegular, fpdfx.FontSize)
	p.SetHeaderFunc(func() { header(p, i) })
	p.SetFooterFunc(func() { footer(p, i) })

	p.AddPage()
	p.SetFont(fpdfx.Font, fpdfx.FontStyleRegular, fpdfx.FontSize)
	clientInvoiceInfo(p, i)
	invoiceTable(p, i)

	invPath := filepath.Join(
		*outDir,
		fmt.Sprintf(
			"%d-%s-%d%d%d.pdf",
			i.Number,
			strings.Replace(i.Client.Name, " ", "", -1),
			i.Date.Day(),
			i.Date.Month(),
			i.Date.Year(),
		),
	)
	return p.OutputFileAndClose(invPath)
}

func header(p *gofpdf.Fpdf, i *invoice.Invoice) {
	l, t, _, _ := p.GetMargins()
	_, h := logo(p, 45.0)

	// go back to page start
	p.SetX(l)
	p.SetY(t)

	requisites(p, &i.Dealer)

	p.SetY(t + h + fpdfx.HeaderBottomMargin)
}

func footer(p *gofpdf.Fpdf, i *invoice.Invoice) {
	_, _, _, b := p.GetMargins()
	_, h := p.GetPageSize()
	_, lh := p.GetFontSize()

	lines := []string{
		fmt.Sprintf("Make all checks payable to %s", i.Dealer.Name),
		strings.ToUpper("Thank you for your business"),
	}

	p.SetY(h - (b + (float64(len(lines)) * lh)))

	for _, line := range lines {
		p.CellFormat(
			0,
			fpdfx.LineHeight,
			line,
			fpdfx.BorderEmpty,
			0,
			fpdfx.AlignCenter,
			false,
			0,
			"",
		)
		fpdfx.NewLn(p)
	}
}

func logo(p *gofpdf.Fpdf, ratio float64) (float64, float64) {
	// obtain page details
	w, _ := p.GetPageSize()
	_, t, r, _ := p.GetMargins()

	img := "logo"
	opts := gofpdf.ImageOptions{ImageType: "JPG", ReadDpi: true}

	// load image to obtain sizes
	p.RegisterImageOptionsReader(img, opts, fpdfx.NewLogoReader())
	info := p.RegisterImageOptions(img, opts)
	scale := info.Height() / ratio

	// calculate scaled sizes based on ratio
	sw := info.Width() / scale
	sh := info.Height() / scale

	// calculate X position of image
	imX := w - r - sw

	// draw
	p.ImageOptions(img, imX, t, 0, ratio, true, opts, 0, "")

	return sw, sh
}

func requisites(p *gofpdf.Fpdf, bp *invoice.BusinessParty) {
	phones := ""

	if len(bp.PhoneNumber1) > 0 {
		phones += bp.PhoneNumber1
	}

	if len(bp.PhoneNumber2) > 0 {
		if phones != "" {
			phones += ", "
		}
		phones += bp.PhoneNumber2
	}

	for _, line := range []string{
		bp.Name,
		bp.Street,
		fmt.Sprintf("%s, %s", bp.City, bp.PostalCode),
		phones,
		bp.Email,
	} {
		fpdfx.WriteLn(p, line)
	}
	fpdfx.NewLn(p)
}

func clientInvoiceInfo(p *gofpdf.Fpdf, i *invoice.Invoice) {
	// initial position
	y := p.GetY()

	// page data
	w, _ := p.GetPageSize()
	l, _, r, _ := p.GetMargins()

	// print client info
	p.SetFont(fpdfx.Font, fpdfx.FontStyleBold, fpdfx.FontSize)
	fpdfx.WriteLn(p, "Bill To: ")
	p.SetFont(fpdfx.Font, fpdfx.FontStyleRegular, fpdfx.FontSize)
	requisites(p, &i.Client)

	// return to this point after invoice data
	y2 := p.GetY()

	p.SetY(y)

	// print invoice data
	lines := []string{
		fmt.Sprintf("Invoice No.: %d", i.Number),
		fmt.Sprintf(
			"Date: %d/%d/%d",
			i.Date.Day(),
			i.Date.Month(),
			i.Date.Year(),
		),
	}

	p.SetFont(fpdfx.Font, fpdfx.FontStyleBold, fpdfx.FontSize)
	for _, line := range lines {
		p.SetX((w - l - r) / 2)
		p.CellFormat(
			0,
			fpdfx.LineHeight,
			line,
			fpdfx.BorderEmpty,
			0,
			fpdfx.AlignRight,
			false,
			0,
			"",
		)
		fpdfx.NewLn(p)
	}

	p.SetY(y2)
	p.SetX(l)
}

func invoiceTable(p *gofpdf.Fpdf, i *invoice.Invoice) {
	// column widths
	col1, col2, col3, col4, col5 := 0.05, 0.60, 0.05, 0.15, 0.15

	// print header cells
	header := [][]fpdfx.TableCell{{
		{Width: col1, Text: "No", Style: fpdfx.HeaderStyle},
		{Width: col2, Text: "Description", Style: fpdfx.HeaderStyle},
		{Width: col3, Text: "Qty", Style: fpdfx.RightStyle},
		{Width: col4, Text: "Unit Price", Style: fpdfx.RightStyle},
		{Width: col5, Text: "Line Total", Style: fpdfx.RightStyle},
	}}
	p.SetFont(fpdfx.Font, fpdfx.FontStyleBold, fpdfx.FontSize)
	fpdfx.Table(p, header)

	// print services cells
	body := make([][]fpdfx.TableCell, 0, 0)
	for index, service := range i.Items {
		body = append(body, []fpdfx.TableCell{
			{
				Width: col1,
				Text:  fmt.Sprintf("%d", index+1),
				Style: fpdfx.BodyStyle,
			},
			{
				Width: col2,
				Text:  service.Description,
				Style: fpdfx.BodyStyle,
			},
			{
				Width: col3,
				Text:  strconv.Itoa(service.Amount),
				Style: fpdfx.RightStyle,
			},
			{
				Width: col4,
				Text:  fmt.Sprintf("%.2f", service.UnitPrice),
				Style: fpdfx.RightStyle,
			},
			{
				Width: col5,
				Text:  fmt.Sprintf("%.2f", service.Total),
				Style: fpdfx.RightStyle,
			},
		})
	}
	p.SetFont(fpdfx.Font, fpdfx.FontStyleRegular, fpdfx.FontSize)
	fpdfx.Table(p, body)
	fpdfx.NewLn(p)

	// summary column widths
	sCol1, sCol2 := 0.85, 0.15

	// print summary cells
	summary := [][]fpdfx.TableCell{
		{
			{
				Width: sCol1,
				Text:  "Subtotal",
				Style: fpdfx.EmptyStyle,
			},
			{
				Width: sCol2,
				Text:  fmt.Sprintf("%.2f", i.SubTotal),
				Style: fpdfx.RightStyle,
			},
		},
		{
			{
				Width: sCol1,
				Text:  fmt.Sprintf("Sales tax @ %.2f%%", i.Tax),
				Style: fpdfx.EmptyStyle,
			},
			{
				Width: sCol2,
				Text:  fmt.Sprintf("%.2f", i.TaxTotal),
				Style: fpdfx.RightStyle,
			},
		},
		{
			{
				Width: sCol1,
				Text:  "Total",
				Style: fpdfx.EmptyStyle,
			},
			{
				Width: sCol2,
				Text:  fmt.Sprintf("%.2f", i.Total),
				Style: fpdfx.RightStyle,
			},
		},
	}
	fpdfx.Table(p, summary)
}
